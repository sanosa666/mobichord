# Angular Test Task

### Installing

Clone repository in your directory
```
git clone https://gitlab.com/sanosa666/mobichord.git
```

In root directory run 
```
npm install
npm start
```

Check app in browser by link  `http://localhost:8000`