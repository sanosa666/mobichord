var basicRequestConfig = {
    method: 'GET',
    headers: {
        'Accept': 'application/json, application/xml',
        'Content-Type': 'application/json, application/xml',
        'Authorization': 'Basic '+btoa(instanceLogin+':'+instancePass)
    }

};
var basicUrl = proxi+'/'+instanceURL;

app.factory('incidents', ['$http', function($http) {
    var requestConfig = basicRequestConfig;

    function createURL(params){
        requestConfig.url = basicUrl+'/api/now/table/incident?sysparm_limit=' + params.limit+
            '&sysparm_offset='+ params.start +'&sysparm_query=ORDERBY';
        if(params.sort !== '')
            requestConfig.url += params.sort;
        requestConfig.url += params.order;
    }

    return function(params){
        createURL(params);
        return $http(requestConfig)
                .success(function(data) {
                    return data;
                })
                .error(function(data) {
                    return data;
                })
    };
}]);

app.factory('incident', ['$http', function($http) {
    var requestConfig = basicRequestConfig;

    function createURL(params){
        requestConfig.url = basicUrl+'/api/now/table/incident/'+params.sys_id;
    }

    return function(params){
        createURL(params);
        return $http(requestConfig)
            .success(function(data) {
                return data;
            })
            .error(function(data) {
                return data;
            })
    };
}]);

app.factory('itemByURL', ['$http', function($http) {
    var requestConfig = basicRequestConfig;

    function createURL(params){
        requestConfig.url = proxi+'/'+params.url;
    }

    return function(params){
        createURL(params);
        return $http(requestConfig)
            .success(function(data) {
                return data;
            })
            .error(function(data) {
                return data;
            })
    };
}]);




// var requestConfig = {
//     method: 'POST',
//     url: 'https://cors-anywhere.herokuapp.com/https://dev10732.service-now.com/oauth_token.do',
//     headers: {
//         'Accept': 'application/json, application/xml',
//         'Content-Type': 'application/json, application/xml'
//     },
//     data: {
//         grant_type: 'password',
//         client_id: 'ac0dd3408c1031006907010c2cc6ef6d',
//         client_secret: 'vlpoayhlauqwa173gsva',
//         username: 'abel.tuter',
//         password: '123123'
//     }
// };