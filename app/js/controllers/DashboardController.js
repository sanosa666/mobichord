app.controller('DashboardController', ['$scope', 'incidents', DashboardCtrl]);

function DashboardCtrl($scope, incidents){
    $scope.setTab('dashboard');

    var params = {
        'start': 0,
        'limit': 100
    };

    $scope.preload = false;
    incidents(params).success(function(data){
        $scope.preload = true;
        $scope.incidents = data.result;
        chartOneBiuld($scope.incidents);
        chartTwoBiuld($scope.incidents);
        chartThreeBiuld($scope.incidents);
        chartFourBiuld($scope.incidents);
    });

    function chartOneBiuld(data){
        statusesArr = {1:0, 2:0, 3:0, 6:0, 7:0, 8:0};
        for(var key in data){
            statusesArr[parseInt(data[key]['state'])]++;
        }

        $scope.chartOne = {
            labels: ["New", "In Progress", "On Hold", "Resolved", "Closed", "Canceled"],
            data: [statusesArr[1], statusesArr[2], statusesArr[3], statusesArr[6], statusesArr[7],statusesArr[8]],
            colors: ['#F7464A', '#FDB45C', '#FD8E2D', '#46BFBD', '#DCDCDC', '#949FB1'],
            show: true
        };

    }
    function chartTwoBiuld(data){

        categoriesArr = {
            '': 0,
            'inquiry': 0,
            'software': 0,
            'hardware': 0,
            'network': 0,
            'database': 0
        };

        for(var key in data){
            categoriesArr[data[key]['category']]++;
        }

        var ChartData = [];
        for(key in categoriesArr){
            ChartData.push(categoriesArr[key]);
        }

        $scope.chartTwo = {
            labels: ['None Category','Inquiry / Help','Software','Hardware','Network','Database'],
            data: ChartData,
            colors: [ '#97BBCD', '#FD8E2D', '#F7464A','#46BFBD', '#DCDCDC', '#949FB1'],
            show: true
        };

    }
    function chartThreeBiuld(data){
        var statusesArr = {};
        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];
        for(var key in data){
            var date = new Date(data[key]['opened_at']);
            var chartStr = monthNames[date.getMonth()]+', '+date.getFullYear();
            if(statusesArr[chartStr] !== undefined){
                statusesArr[chartStr]['count']++;
            } else {
                statusesArr[chartStr] = {
                    count: 1,
                    dateStr: chartStr,
                    timestamp: date.getTime()
                };
            }
        }

        var sortable = [];
        for (key in statusesArr) {
            sortable.push([key, statusesArr[key]['timestamp'], statusesArr[key]['count']]);
        }
        sortable.sort(function(a, b) {
            return a[1] - b[1];
        });

        var labels = [];
        var chartData = [];
        for(key in sortable){
            labels.push(sortable[key][0]);
            chartData.push(sortable[key][2]);
        }

        $scope.chartThree = {
            labels: labels,
            data: chartData,
            series: ['Incidents'],
            show: true
        };
    }
    function chartFourBiuld(data){
        var periodsArr = {'1-2 days':0, '2-7 days':0, '2-4 weeks':0, '> 1 month':0};
        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];
        for(var key in data){
            if(data[key]['state'] !== '7') continue;

            var dateStar = new Date(data[key]['opened_at']);
            var dateEnd = new Date(data[key]['closed_at']);
            var miliseconds = dateEnd - dateStar;
            var days = Math.ceil(miliseconds/1000/60/60/24);

            if(days <= 2){
                periodsArr['1-2 days']++;
            } if (days <= 7){
                periodsArr['2-7 days']++;
            } if(days <= 30 ){
                periodsArr['2-4 weeks']++;
            } else {
                periodsArr['> 1 month']++;
            }
        }

        var labels = [];
        var chartData = [];
        for(key in periodsArr){
            labels.push(key);
            chartData.push(periodsArr[key]);
        }

        $scope.chartFour = {
            labels: labels,
            data: chartData,
            series: ['Incidents'],
            show: true
        };

    }
}

