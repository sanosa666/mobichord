app.controller('ListController', ['$scope', 'incidents', ListCtrl]);
function ListCtrl($scope, incidents) {

    $scope.setTab('list');
    var params = {
        'start': 0,
        'limit': 20,
        'order': 'number',
        'sort': 'DESC'
    };

    $scope.sort = function(column){
        var elClass = 'asc';
        if(params.order === column){
            if(params.sort === '') {
                params.sort = 'DESC';
                elClass = 'desc'
            }
            else {
                params.sort = '';
            }
        } else {
            params.order = column;
            params.sort = '';
        }
        angular.element(document.querySelector(".incidentsList")).find('thead').find('td').removeClass('order');
        angular.element(document.querySelector(".incidentsList ."+column)).addClass('order').find('span').attr('class',elClass);

        loadItems();
    };
    $scope.more = function(){
        params.limit += 20;

        loadItems();
    };
    $scope.openItem = function(sys_id){
        var newURL = '/#/list/'+sys_id;
        location.href = newURL;
    }

    $scope.sort('number');

    function loadItems(){
        $scope.preload = false;
        incidents(params).success(function(data){
            if(data['result'].length < params.limit)
                $scope.noMore = true;
            $scope.preload = true;
            $scope.incidents = prepareArray(data['result']);
        });
    }
    function prepareArray(result){
        var returnArr = [];
        for(var key in result){

            var priority = 0;
            switch (parseInt(result[key].priority)){
                case 1: priority = 'Critical'; break;
                case 2: priority = 'High'; break;
                case 3: priority = 'Moderate'; break;
                case 4: priority = 'Low'; break;
                case 5: priority = 'Planning'; break;
            }
            var state = 0;
            switch (parseInt(result[key].state)){
                case 1: state = 'New'; break;
                case 2: state = 'In Progress'; break;
                case 3: state = 'On Hold'; break;
                case 6: state = 'Resolved'; break;
                case 7: state = 'Closed'; break;
                case 8: state = 'Canceled'; break;
            }
            returnArr.push({
                'sys_id':    result[key].sys_id,
                'number':    result[key].number,
                'category':  result[key].category,
                'priority':  priority,
                'state':     state,
                'opened_at': new Date(result[key].opened_at)
            })
        }
        return returnArr;
    }
}