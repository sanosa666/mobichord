app.controller('ItemController', ['$scope', '$routeParams', 'incident', 'itemByURL', ItemCtrl]);

function ItemCtrl($scope, $routeParams, incident, itemByURL) {

    incident({sys_id:$routeParams.id}).success(function(data){
        $scope.preload = true;
        $scope.incident = data['result'];
        $scope.incident = numberToWord($scope.incident);

        for(var key in $scope.incident){
            if(typeof $scope.incident[key] === 'object' ){
                let locKey = key;
                itemByURL({url: $scope.incident[locKey]['link']}).success(function(data){
                    $scope.incident[locKey] = data['result'];

                });
            }
        }

        $scope.setIncident({name:data['result'].number, sys_id:$routeParams.id});
        $scope.showCeilLoader = function(ceil){
            if($scope.preload === true){
                if(ceil === undefined){
                    return true;
                }
            }
            return false;
        }
    });
    function numberToWord($array){
        var state = 0;
        switch (parseInt($array.state)){
            case 1: state = 'New'; break;
            case 2: state = 'In Progress'; break;
            case 3: state = 'On Hold'; break;
            case 6: state = 'Resolved'; break;
            case 7: state = 'Closed'; break;
            case 8: state = 'Canceled'; break;
        }
        $array.state = state;

        var priority = 0;
        switch (parseInt($array.priority)){
            case 1: priority = 'Critical'; break;
            case 2: priority = 'High'; break;
            case 3: priority = 'Moderate'; break;
            case 4: priority = 'Low'; break;
            case 5: priority = 'Planning'; break;
        }
        $array.priority = priority;

        var urgency = 0;
        switch (parseInt($scope.incident.urgency)){
            case 1: urgency = 'High'; break;
            case 2: urgency = 'Medium'; break;
            case 3: urgency = 'Low'; break;
        }
        $array.urgency = urgency;

        var impact = 0;
        switch (parseInt($scope.incident.impact)){
            case 1: impact = 'High'; break;
            case 2: impact = 'Medium'; break;
            case 3: impact = 'Low'; break;
        }
        $array.impact = impact;

        return $array;
    }
    $scope.setTab('listItem');


}