app.controller('NavController', ['$scope','$routeParams',  NavCtrl]);

function NavCtrl($scope, $routeParams){

    $scope.setTab = function(tabName){
        $scope.currentNavItem = tabName;
    };
    $scope.setIncident = function(_incident){
        $scope.incident = {
            name: _incident.name,
            sys_id: _incident.sys_id,
            show: true
        }
    };
    $scope.incident = {
        name: '',
        sys_id: '',
        show: false
    }

}