var app = angular.module('Dashboard', ['ngRoute', 'ngMaterial', "ngAnimate", "ngAria", "chart.js"]);

app.config(function ($routeProvider, $locationProvider) {
    $routeProvider
        .when('/', {
            // controller: 'DashboardController',
            templateUrl: 'views/dashboard.html'
        })
        .when('/list', {
            // controller: 'ListController',
            templateUrl: 'views/list.html'
        })
        .when('/list/:id', {
            // controller: 'ItemController',
            templateUrl: 'views/item.html'
        })
        .otherwise({
            redirectTo: '/'
        });
});